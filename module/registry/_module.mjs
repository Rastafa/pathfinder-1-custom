export { BaseRegistry, BaseRegistryObject } from "./base-registry.mjs";
export { damageTypes, DamageType, DamageTypes } from "./damage-types.mjs";
export { ScriptCall, ScriptCalls, scriptCalls } from "./script-call.mjs";
