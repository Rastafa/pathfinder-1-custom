{
  "_id": "wVpQnwScicfnKl1l",
  "name": "Osirion Mummy",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template</strong> Acquired<br><strong>Simple Template </strong>No<br><strong>Usable with Summons</strong> No<p>The mummification process of ancient Osirion results in a variant mummy. Although the Osirion mummy appears very similar to normal mummies-a desiccated husklike creature, draped in embalming wrap adorned with hieroglyphics-the Osirion mummy differs slightly in ability. Osirion mummies do not spread the curse of mummy rot through touch, nor does the very sight of them give rise to paralysis. They are still, however, resilient killing machines.<p>\"Osirion mummy\" is an acquired template that can be added to any living, corporeal creature (hereafter referred to as the base creature). An Osirion mummy uses all of the base creature's statistics except as noted here.<p><strong>Size and Type:</strong> The creature's type changes to undead, and it gains the augmented subtype. It retains any other subtypes as well, except for alignment subtypes (such as good). Do not recalculate base attack bonuses, saves, or skill points. Size is unchanged.<p><strong>Hit Dice:</strong> Increase all current and future Hit Dice to d12.<p><strong>AC:</strong> An Osirion mummy's natural armor bonus increases by +5.<p><strong>Defensive Abilities: </strong>An Osirion mummy retains the base creature's defensive abilities and gains damage reduction.<p><em>Damage Reduction (Ex) </em>An Osirion mummy's body is resilient, providing it with damage reduction 5/-.<p><strong>Weaknesses:</strong> An Osirion mummy retains the base creature's weaknesses and gains energy vulnerability.<p><em>Energy vulnerability (Ex)</em> The mummification process leaves the mummy vulnerable to a single energy type, from which it takes half again as much damage (+50%) as normal. Choose or determine randomly from the following list:</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>d10</strong></td>\n<td><strong>Energy</strong></td>\n</tr>\n<tr>\n<td>1-4</td>\n<td>Fire</td>\n</tr>\n<tr>\n<td>5-6</td>\n<td>Acid</td>\n</tr>\n<tr>\n<td>7-8</td>\n<td>Cold</td>\n</tr>\n<tr>\n<td>9</td>\n<td>Electricity</td>\n</tr>\n<tr>\n<td>10</td>\n<td>Sonic</td>\n</tr>\n</tbody>\n</table>\n<p>As an emergency safeguard, it was common for the necromantic embalmers of ancient Osirion to subtly mark the particular energy type to which the mummy would be vulnerable with a separate hieroglyph someplace inconspicuously on the mummy's body or wrappings. A DC 20 Spot check uncovers the mark, but unless the viewer is capable of comprehending its meaning a DC 20 Decipher Script check is required to unlock its secret.<p><strong>Speed: </strong>An Osirion mummy's speeds all decrease by 10 feet (minimum 5 feet). If the base creature has a flight speed its maneuverability class worsenes by one step, to a minimum of clumsy.<p><strong>Attack:</strong> An Osirion mummy retains all the attacks of the base creature and also gains a slam attack if it did not already have one. If the base creature can use weapons, the Osirion mummy retains that ability. In addition, all of an Osirion mummy's attacks are treated as magical for the purpose of overcoming damage reduction.<p><strong>Damage:</strong> The mummification process hardens the mummy's bones to a stonelike density, granting the mummy a powerful slam attack. The creature's slam attack deals damage according to its size as listed below.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Size</strong></td>\n<td><strong>Damage</strong></td>\n</tr>\n<tr>\n<td>Fine</td>\n<td>1</td>\n</tr>\n<tr>\n<td>Diminutive</td>\n<td>1d2</td>\n</tr>\n<tr>\n<td>Tiny</td>\n<td>1d3</td>\n</tr>\n<tr>\n<td>Small</td>\n<td>1d4</td>\n</tr>\n<tr>\n<td>Medium</td>\n<td>1d6</td>\n</tr>\n<tr>\n<td>Large</td>\n<td>1d8</td>\n</tr>\n<tr>\n<td>Huge</td>\n<td>2d6</td>\n</tr>\n<tr>\n<td>Gargantuan</td>\n<td>2d8</td>\n</tr>\n<tr>\n<td>Colossal</td>\n<td>4d6</td>\n</tr>\n</tbody>\n</table>\n<p><strong>Special Attacks: </strong>An Osirion mummy retains the base creature's special attacks and also gains the following.<p><em>Dust Stroke (Su) </em>A successful natural or slam attack by an Osirion mummy that drops its victim's hit points to below -9 does more than just kill the victim, it also disintegrates the victim's body into a cloud of dust and ash. A raise dead spell cannot bring back the victim, but a resurrection still works.<p><em>Sudden Burst of Vengeance (Su) </em>Despite its slow lumbering nature, an Osirion mummy is capable of lurching forward to attack with a short but surprising, explosion of speed. Twice per day, as a free action, an Osirion mummy may act as though augmented by a haste spell. The effect lasts for a single round.<p><strong>Abilities: </strong>An Osirion mummy's ability scores are modified as follows: Str +4, Int -2 (minimum 1). As an undead creature, an Osmon mummy has no Constitution score.<p><strong>Feats: </strong>The creature gains Improved Natural Attack for each natural attack form as a bonus feat. If the creature previously had a slam attack before adding the template. the creature's new slam attack also gains the Improved Natural Attack feat.<p><strong>Environment: </strong>Any.<p><strong>Challenge Rating:</strong> As base creature +1.<p><strong>Alignment:</strong> Usually lawful evil.</p>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "1rorl0sn",
        "formula": "5",
        "operator": "add",
        "subTarget": "nac",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ac"
      },
      {
        "_id": "8e9br02f",
        "formula": "-10",
        "operator": "add",
        "subTarget": "allSpeeds",
        "modifier": "base",
        "priority": 0,
        "value": 0,
        "target": "speed"
      },
      {
        "_id": "pb56y66m",
        "formula": "4",
        "operator": "add",
        "subTarget": "str",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "wmnyx188",
        "formula": "-2",
        "operator": "add",
        "subTarget": "int",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    },
    "crOffset": "1"
  }
}
