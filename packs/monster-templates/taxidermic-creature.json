{
  "_id": "qoHQEdnaA5CdZEkn",
  "name": "Taxidermic Creature",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template</strong> Acquired<br><strong>Simple Template</strong> No<br><strong>Usable with Summons</strong> No<p>Taxidermic creatures are the work of obsessed individuals seeking to use their alchemical or occult talents to preserve and reanimate lifeless beings. The abilities of taxidermic creatures often pale in comparison to those of their living counterparts, as they are bereft of intelligence or an animate life force to guide them. Instead, taxidermic creatures possess a rudimentary form of instinct, though they are still able to follow basic instructions from their creators.<p>Less refined than the magic used to animate undead, golems, and other constructs, the creation of a taxidermic creature is at best an inaccurate science. There is no one method of crafting a taxidermic creature, so each result is different. Every creature crafted in such a manner is prone to inherent defects based on the materials used or shortcuts taken during the process, and may or may not be able to obey its creators commands in a satisfactory manner. Because of taxidermic creatures’ limited mental faculties, their creators must be extremely careful and literal when commanding them.<p>Left unattended, taxidermic creatures stand in place, having no need to drink, eat, or sleep. Unless given specific commands, the actions of these creatures are unpredictable. Each taxidermic creature behaves differently, depending on the quirks of its individual construction. Some taxidermic creatures move and act like prowling animals, while others move with the rigidity and unerring purpose of animated objects. Some twisted taxidermists have gone so far as to create taxidermic humanoids, aberrations, and even fey.<p>“Taxidermic creature” is an acquired template that can be added to any corporeal creature (other than constructs or undead), referred to hereafter as the base creature.<p><strong>Challenge Rating:</strong> This depends on the creature’s original number of Hit Dice, as noted on the following table, and is further adjusted based on its size, as noted in the Hit Dice entry below.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Hit Dice</strong></td>\n<td><strong>CR</strong></td>\n</tr>\n<tr>\n<td>1</td>\n<td>1/4</td>\n</tr>\n<tr>\n<td>2</td>\n<td>1/2</td>\n</tr>\n<tr>\n<td>3-4</td>\n<td>1</td>\n</tr>\n<tr>\n<td>5-6</td>\n<td>2</td>\n</tr>\n<tr>\n<td>7-8</td>\n<td>3</td>\n</tr>\n<tr>\n<td>9-10</td>\n<td>4</td>\n</tr>\n<tr>\n<td>11-12</td>\n<td>5</td>\n</tr>\n<tr>\n<td>13-16</td>\n<td>6</td>\n</tr>\n<tr>\n<td>17-20</td>\n<td>7</td>\n</tr>\n<tr>\n<td>21-24</td>\n<td>8</td>\n</tr>\n<tr>\n<td>25-28</td>\n<td>9</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Alignment:</strong> Always neutral.<p><strong>Type:</strong> The creature’s type changes to construct. It retains all subtypes except for alignment subtypes (such as good) and subtypes that indicate kind. It does not gain the augmented subtype. It uses all the base creature’s statistics and special abilities except as noted here.<p><strong>Senses: </strong>The creature gains darkvision 60 feet and lowlight vision if it does not already possess them.<p><strong>Armor Class:</strong> The taxidermic creature’s natural armor bonus is based on its size.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Size</strong></td>\n<td><strong>Natural Armor Bonus</strong></td>\n</tr>\n<tr>\n<td>Tiny or smaller</td>\n<td>+0</td>\n</tr>\n<tr>\n<td>Small</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>Medium</td>\n<td>+2</td>\n</tr>\n<tr>\n<td>Large</td>\n<td>+3</td>\n</tr>\n<tr>\n<td>Huge</td>\n<td>+4</td>\n</tr>\n<tr>\n<td>Gargantuan</td>\n<td>+7</td>\n</tr>\n<tr>\n<td>Colossal</td>\n<td>+11</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Hit Dice:</strong> Remove Hit Dice gained from class levels (minimum of 1) and change all racial Hit Dice to d10s. As constructs, taxidermic creatures gain a number of bonus hit points based on their size. This information is repeated in the table below. Taxidermic creatures also gain bonus Hit Dice based on their size, as noted on the following table.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Size</strong></td>\n<td><strong>Bonus Hit Dice</strong></td>\n<td><strong>Bonus Construct hp</strong></td>\n<td><strong>CR Increase</strong></td>\n</tr>\n<tr>\n<td>Tiny or smaller</td>\n<td>—</td>\n<td>—</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Small</td>\n<td>—</td>\n<td>+10</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Medium</td>\n<td>—</td>\n<td>+20</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Large</td>\n<td>+1 HD</td>\n<td>+30</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>Huge</td>\n<td>+2 HD</td>\n<td>+40</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>Gargantuan</td>\n<td>+3 HD</td>\n<td>+60</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>Colossal</td>\n<td>+4 HD</td>\n<td>+80</td>\n<td>+2</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Saves:</strong> The creature’s base save bonuses are Fortitude +1/3 Hit Dice, Reflex +1/3 Hit Dice, and Will +1/3 Hit Dice.<p><strong>Defensive Abilities:</strong> Taxidermic creatures lose their defensive abilities and gain all the qualities and immunities granted by the construct type.<p><strong>Weaknesses:</strong> A taxidermic creature gains the following weakness.<p><em>Defect (Ex):</em> Every taxidermic creature has one major defect. This manifests as a unique weakness, based on the reconstruction method used on the subject. Select one of the following.</p>\n<ul>\n<li>Crude Stitching: Massive open stitches mar the surface of the taxidermic creature. It gains vulnerability to slashing weapons.</li>\n<li>Defective Eyes: Whether from cracks in a glass eye or cloudiness from preservatives applied too late, the eyes on the taxidermic creature are damaged. Every attack, melee or ranged, made by the taxidermic creature suffers a 10% miss chance.</li>\n<li>Understuffed: Reduce the number of bonus hit points the taxidermic creature receives from being a construct by half. Only taxidermic creatures of Medium size or smaller can have this defect.</li>\n<li>Wire Frame: Thick metal wiring supports the taxidermic creature. It is vulnerable to electricity and counts as metal for the purposes of abilities and spells that affect metal (such as <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=chill%20metal\">chill metal</a> or <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=shocking%20grasp\">shocking grasp</a>).</li>\n<li>Wooden Struts: Planks of wood hold up the taxidermic creature’s frame. It is vulnerable to fire and counts as wood for the purposes of abilities and spells that affect wood (such as <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=warp%20wood\">warp wood</a>).</li>\n</ul>\n<p><br><strong>Speed:</strong> Reduce the base speed of a taxidermic creature by 10 feet, to a minimum of 20 feet. Winged taxidermic creatures can fly, but their maneuverability drops to clumsy. If the base creature flew magically, the taxidermic creature loses this ability. Retain all other movement types.<p><strong>Attacks:</strong> A taxidermic creature retains all natural weapons, manufactured weapon attacks, and weapon proficiencies of the base creature. It also gains a slam attack that deals damage based on the taxidermic creature’s size.<p><strong>Special Attacks:</strong> A taxidermic creature retains none of the base creature’s special attacks.<p><strong>Ability Scores:</strong> Strength –2, Dexterity –2. A taxidermic creature has no Constitution or Intelligence score. Its Wisdom becomes 10 and Charisma becomes 3.<p><strong>Base Attack Bonus: </strong>A taxidermic creature’s base attack bonus is equal to 3/4 of its Hit Dice, even though most constructs have base attack bonuses equal to their Hit Dice.<p><strong>Skills: </strong>A taxidermic creature has no skill ranks. It loses all racial bonuses on skill checks that are not directly related to its physical form.<p><strong>Feats:</strong> A taxidermic creature loses all feats possessed by the base creature, and does not gain feats as its Hit Dice increase.<p><strong>Special Qualities:</strong> A taxidermic creature loses most special qualities of the base creature. It retains any extraordinary special qualities that improve its melee or ranged attacks.</p>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "1zxhv0z1",
        "formula": "-2",
        "operator": "add",
        "subTarget": "str",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "hvn360hs",
        "formula": "-2",
        "operator": "add",
        "subTarget": "dex",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    }
  }
}
